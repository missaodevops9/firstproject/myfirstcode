"""

Modulo principal.

"""
from HelloWorld import Hello

def main():
   Hello.hello()

if __name__ == "__main__":
    main()
